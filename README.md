# KI-Campus API Module

This module exposes the KI-Campus API for accessing course and user data.

You can find the documentation of the API [here](doc/api.md).
