# Changelog

The versions in this changelog have nothing to do with the version passed in the
Accept headers. This is merely an enumeration of the changes for easier
reference and tracking.

## v1.1

* Add new key `backend_course_data` to the data provided by
  `/kic_api/course/UUID`.
