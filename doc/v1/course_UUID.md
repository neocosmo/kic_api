# GET /kic_api/course/UUID

Retrieve course information.

## Path parameters

### `UUID`

The Drupal UUID or KIC UUID of a course.

#### Example

`00112233-4455-6677-8899-aabbccddeeff`


## Header parameters

### `Accept`

Define the media type and API version.

You MUST use `application/vnd.kicampus.course+json;v=1.0`.


## Response

A keyed JSON object of the course or an error.

The following information are provided:
* `title` (String)
* `language` (String: IETF BCP 47 language tag)
* `id` (String: UUID)
* `abstract` (String)
* `description` (String)
* `ects` (String: Integer)
* `duration` (String: Integer)
* `target_group` (Enum)
* `achievement_record` (Enum)
* `supplier` (Enum)
* `format` (Enum)
* `level` (Enum)
* `subject_group` (Array of enum)
* `occupational_field` (Array of enum)
* `external_knownledge` (Array of enum)
* `social_learning` (Array of enum)
* `start_date` (String: ISO 8601 date)
* `end_date` (String: ISO 8601 date)
* `institution` (String: UUID)
* `lecturers` (Array of String: UUID)
* `backend_course_data` (Object)

*String* may be an arbitrary string.\
*String: ...* is a string following the specified format.\
*Enum* is a string from a predefined set.

The `backend_course_data` is the course data as retrieved from the learning
backend.

*Example:*
```json
{
    "abstract": "Lorem ipsum.",
    "achievement_record": "cop",
    "description": "<p>Lorem ipsum.</p>",
    "duration": "",
    "ects": "5",
    "end_date": "2021-12-31T12:00:00+00:00",
    "external_knownledge": [
      "medicine"
    ],
    "format": "",
    "id": "00112233-4455-6677-8899-aabbccddeeff",
    "institution": "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee",
    "language": "de",
    "lecturers": [
      "01234567-89ab-cdef-0123-456789abcdef"
    ],
    "level": "advanced",
    "occupational_field": [
      "healthcare"
    ],
    "social_learning": [],
    "start_date": "2021-01-01T15:00:00+00:00",
    "subject_group": [],
    "supplier": "kicampus",
    "target_group": "apply",
    "title": "Example Course"
}
```

### Errors

TODO
