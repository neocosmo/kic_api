# GET /kic_api/course_list

Retrieve a list of all courses.

## Path parameters

None.

## Header parameters

### `Accept`

Define the media type and API version.

You MUST use `application/vnd.kicampus.course+json;v=1.0`.


## Response

A JSON array of objects. Each objects has the following fields:
* `drupal_uuid` (String): UUID of the course used by Drupal.
* `kic_uuid` (String): UUID of the course used by the learning backend. This
  might be `null`.


### Errors

TODO
