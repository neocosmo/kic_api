# KI-Campus API v1

[Changelog](changelog.md)

## Course information

* [/kic_api/course_list](course_list.md)
* [/kic_api/course/UUID](course_UUID.md)
* [/kic_api/users](users.md)
* [/kic_api/user/UUID](user_UUID.md)
