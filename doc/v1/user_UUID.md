# GET /kic_api/users/UUID

Get information about a user.


## Path parameters

### `UUID`

The UUID of the user.

#### Example

`a419f485-2a3e-8825-9a5e-8c4e1f3e8424`


## Query parameters

### `since`

A positive integer representing a UNIX timestamp. If provided, only those user
information gathered since that point in time are returned.


## Header parameters

### `Accept`

Define the media type and API version.

You MUST use `application/vnd.kicampus.user+json;v=1.0`.


## Response

A JSON object of user information. The object has the following attributes:

* `course_visits`: An array of objects. Each object has the attributes `uuid`
  and `timestamps`. `uuid` is a string of the course's UUID. `timestamps` is an
  array of UNIX timestamps of when then user visited the course's page.
* `enrollments`: An array of objects, each with the `uuid` attribute. The UUID
  is that of the course the user enrolled to.
* `search_terms`: An array of objects. Each object comprises the attributes
  `search_term` and `timestamps`. `search_term` is a string of the term the user
  searched and `timestamps` is an array of UNIX timestamps at which the user
  search for the term.
* `saml_id`: The user's ID used within the SAML SSO process to authenticate
  against the learning backend.

*Example:*
```json
{
    "course_visits": [
        {
            "timestamps": [
                "1637081336"
            ],
            "uuid": "22218451-41f0-23dd-c50f-cdd8096610c1"
        }
    ],
    "enrollments": [
        {
            "uuid": "22218451-41f0-23dd-c50f-cdd8096610c1"
        }
    ],
    "search_terms": [
        {
            "search_term": "K\u00fcnstliche Intelligenz",
            "timestamps": [
                "1637080125"
            ]
        },
        {
            "search_term": "Schule",
            "timestamps": [
                "1637228852"
            ]
        }
    ],
    "saml_id": "65d3824d70ec232172cba7337fb93addfd262a6a"
}
```

### Errors

TODO
