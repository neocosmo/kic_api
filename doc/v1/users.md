# GET /kic_api/users

Get a list of all active and enabled users.


## Path parameters

None.


## Query parameters

### `limit`

If the value is a positive integer, the number of results is limited to that
value.

### `start`

If the `limit` query parameter is present, start may be a positive integer that
specifies the offset of the returned results.


## Header parameters

### `Accept`

Define the media type and API version.

You MUST use `application/vnd.kicampus.list+json;v=1.0`.


## Response

A JSON array of one object per active user or an error. The user objects contain
the single attribute `uuid`.

*Example:*
```json
[
    {
        "uuid": "a419f485-2a3e-8825-9a5e-8c4e1f3e8424"
    },
    {
        "uuid": "1133ee2f-52ed-09be-6209-9a95c059b682"
    }
]
```

### Errors

TODO
