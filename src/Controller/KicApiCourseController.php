<?php

namespace Drupal\kic_api\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Controller for KIC-API requests.
 */
class KicApiCourseController extends KicApiGenericController {

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * A module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('serializer'),
      $container->get('module_handler')
    );
  }

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   * @param \Drupal\Core\Database\Connection $dbconn
   *   A database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory to retrieve the module's configuration.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   Entity repository to load course nodes.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager to load course nodes.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   Serializer for serializing courses into JSON.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   A module handler.
   */
  public function __construct(
    RequestStack $request_stack,
    Connection $dbconn,
    ConfigFactoryInterface $config_factory,
    EntityRepositoryInterface $entity_repository,
    EntityTypeManagerInterface $entity_type_manager,
    SerializerInterface $serializer,
    ModuleHandlerInterface $module_handler) {

    parent::__construct($request_stack, $dbconn, $config_factory);
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->serializer = $serializer;
    $this->moduleHandler = $module_handler;
  }

  /**
   * API endpoint callback for /api/course.
   */
  public function course(string $uuid) {
    $version
      = $this->parseAcceptHeader(['application/vnd.kicampus.course+json']);
    if ($version instanceof JsonResponse) {
      return $version;
    }
    /*
     * Use a switch statement for selecting the actual function doing the work
     * so that we do not need to implement a function for every version and
     * simply can fall through, if the API version change did not affect this
     * endpoint.
     */
    switch ($version) {
      case '1.0':
        return $this->courseV1v0($uuid);

      default:
        return $this->invalidAcceptHeaderResponse('Invalid version specified in accept header.' . $method);
    }
  }

  /**
   * API endpoint callback for /api/course v1.0.
   */
  protected function courseV1v0(string $uuid) {
    $node = $this->entityRepository->loadEntityByUuid('node', $uuid);
    $config = $this->configFactory->get('kic_api.settings');
    $kic_uuid_field = $config->get('kic_uuid_field', '');
    if (!$node) {
      if (empty($kic_uuid_field)) {
        return $this->responseNotFound();
      }

      $query = $this->entityTypeManager->getStorage('node')->getQuery();
      $results = $query
        ->condition($kic_uuid_field, $uuid, '=')
        ->range(NULL, 1)
        ->execute();

      $node = Node::load(array_values($results)[0] ?? 0);
    }
    if (!$node) {
      return $this->responseNotFound();
    }
    $node_data = $this->serializer->serialize($node, 'kic_json');
    if ($this->moduleHandler->moduleExists('kic_learn') && $kic_uuid_field
      && $node->hasField($kic_uuid_field)
      && !$node->get($kic_uuid_field)->isEmpty()) {

      $kic_uuid = $node->get($kic_uuid_field)->first()->getValue()['value'] ?? '';
      $api_con = \Drupal::service('kic_learn.api_connector');
      $backend_course = $api_con->getCourse($kic_uuid);
      if ($backend_course) {
        $node_data_decoded = json_decode($node_data);
        $node_data_decoded->backend_course_data = $backend_course;
        $node_data = json_encode($node_data_decoded);
      }
    }
    return Response::create($node_data);
  }

  /**
   * Create a JSON response for not finding the requested node.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JsonResponse with the not-found status.
   */
  protected function responseNotFound() {
    return JsonResponse::create([
      'type' => 'not_found',
      'title' => 'UUID not found.',
      'status' => 404,
    ], 404);
  }

}
