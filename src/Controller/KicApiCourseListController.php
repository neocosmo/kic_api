<?php

namespace Drupal\kic_api\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for KIC-API requests.
 */
class KicApiCourseListController extends KicApiGenericController {

  /**
   * API endpoint callback for /api/course_list.
   */
  public function courseList() {
    $version
      = $this->parseAcceptHeader(['application/vnd.kicampus.course+json']);
    if ($version instanceof JsonResponse) {
      return $version;
    }
    /*
     * Use a switch statement for selecting the actual function doing the work
     * so that we do not need to implement a function for every version and
     * simply can fall through, if the API version change did not affect this
     * endpoint.
     */
    switch ($version) {
      case '1.0':
        return $this->courseListV1v0();

      default:
        return $this->invalidAcceptHeaderResponse('Invalid version specified in accept header.' . $method);
    }
  }

  /**
   * API endpoint callback for /api/course_list v1.0.
   */
  protected function courseListV1v0() {
    $config = $this->configFactory->get('kic_api.settings');

    // @todo Include all course types.
    $query = $this->dbconn->select('node', 'n')
      ->orderBy('n.nid', 'ASC')
      ->condition('n.type', $config->get('course_node_type'), '=')
      ->distinct();
    $query->addField('n', 'uuid', 'drupal_uuid');
    $nfd_alias = $query->join('node_field_data', 'nfd', 'nfd.nid = n.nid AND nfd.vid = n.vid');
    $query->condition($nfd_alias . '.status', 1, '=');

    $kic_uuid_field = $config->get('kic_uuid_field', 'none');
    if ($kic_uuid_field !== 'none') {
      $nuuid_alias = $query->leftJoin('node__' . $kic_uuid_field, 'nuuid',
        'nuuid.entity_id = n.nid AND nuuid.revision_id = n.vid');
      $query->addField($nuuid_alias, 'field_kic_uuid_value', 'kic_uuid');
    }
    $results = $query->execute()->fetchAll();
    return JsonResponse::create($results);
  }

}
