<?php

namespace Drupal\kic_api\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for KIC-API requests.
 */
abstract class KicApiGenericController extends ControllerBase {

  /**
   * Current API version.
   *
   * The API as a whole is versioned with a minor and major version. The minor
   * version is increased everytime the API changes in a backwards compatible
   * way. The major version is increaded everytime the API changes in a non
   * backwards compatible way.
   *
   * @var string
   */
  public static $apiVersion = '1.0';

  /**
   * Current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\JsonResponse
   */
  protected $requestStack;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dbconn;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('database'),
      $container->get('config.factory')
    );
  }

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   * @param \Drupal\Core\Database\Connection $dbconn
   *   A database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory to retrieve the module's configuration.
   */
  public function __construct(
    RequestStack $request_stack,
    Connection $dbconn,
    ConfigFactoryInterface $config_factory) {

    $this->requestStack = $request_stack;
    $this->dbconn = $dbconn;
    $this->configFactory = $config_factory;
  }

  /**
   * Parse the accept header of the current HTTP request.
   *
   * @param array $accepted_mime_types
   *   An array of MIME type strings that are allowed for the endpoint requests.
   *
   * @return string|\Symfony\Component\HttpFoundation\JsonResponse
   *   Return the API version specified in the Accept header or a JsonResponse
   *   on error.
   */
  protected function parseAcceptHeader(array $accepted_mime_types) {
    $headers = $this->requestStack->getCurrentRequest()->headers;
    $accept = $headers->get('accept');

    if (empty($accept)) {
      return $this->invalidAcceptHeaderResponse('The Accept header is missing.');
    }

    $parts = explode(';', $accept);
    if (!empty($accepted_mime_types)
      && !in_array($parts[0], $accepted_mime_types)) {

      return $this->invalidAcceptHeaderResponse('Invalid type specified in accept header.');
    }
    array_shift($parts);

    $version = '';
    foreach ($parts as $part) {
      $part = trim($part);
      if (substr($part, 0, 2) === 'v=') {
        $version = substr($part, 2);
      }
      else {
        return $this->invalidAcceptHeaderResponse('Invalid specifier in accept header.');
      }
    }
    if (empty($version)) {
      return $this->invalidAcceptHeaderResponse('Missing version specification');
    }
    return $version;
  }

  /**
   * Create a JsonResponse for an invalid Accept Header.
   *
   * @param string $text
   *   The error text included in the response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JsonResponse with information about the invalid Accept header.
   */
  protected function invalidAcceptHeaderResponse(string $text) {
    return JsonResponse::create([
      'type' => 'invalid_accept_header',
      'title' => $text,
      'status' => 406,
    ], 406);
  }

}
