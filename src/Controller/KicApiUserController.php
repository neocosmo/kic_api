<?php

namespace Drupal\kic_api\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\kic_learn\KicLearnSimplesamlphpUserId;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for KIC-API user requests.
 */
class KicApiUserController extends KicApiGenericController {

  /**
   * An entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * A module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('logger.factory')
    );
  }

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   * @param \Drupal\Core\Database\Connection $dbconn
   *   A database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory to retrieve the module's configuration.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager to load course nodes.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   A module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory.
   */
  public function __construct(
    RequestStack $request_stack,
    Connection $dbconn,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    LoggerChannelFactoryInterface $logger_factory) {

    parent::__construct($request_stack, $dbconn, $config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Callback for API endpoint /users.
   */
  public function list() {
    $version = $this->parseAcceptHeader(['application/vnd.kicampus.list+json']);
    if ($version instanceof JsonResponse) {
      return $version;
    }
    /*
     * Use a switch statement for selecting the actual function doing the work
     * so that we do not need to implement a function for every version and
     * simply can fall through, if the API version change did not affect this
     * endpoint.
     */
    switch ($version) {
      case '1.0':
        return $this->listV1v0();

      default:
        return $this->invalidAcceptHeaderResponse('Invalid version specified in accept header.' . $method);
    }
  }

  /**
   * Callback for API endpoint /users v1.0.
   */
  protected function listV1v0() {
    $request = $this->requestStack->getCurrentRequest();
    $limit = (int) $request->get('limit', 0);
    $start = (int) $request->get('start', 0);
    $query = \Drupal::entityQuery('user')
      ->condition('access', 0, '<>')
      ->condition('status', 1);
    if ($limit > 0) {
      $query->range($start, $limit);
    }
    $uids = array_keys($query->execute());
    $uuids = $this->dbconn->select('users', 'u')
      ->fields('u', ['uuid'])
      ->condition('uid', $uids, 'IN')
      ->orderBy('uid', 'asc')
      ->execute()
      ->fetchAll();
    $data = $uuids;
    return JsonResponse::create($data);
  }

  /**
   * Callback for API endpoint /users/UUID.
   */
  public function user($uuid) {
    $version = $this->parseAcceptHeader(['application/vnd.kicampus.user+json']);
    if ($version instanceof JsonResponse) {
      return $version;
    }
    /*
     * Use a switch statement for selecting the actual function doing the work
     * so that we do not need to implement a function for every version and
     * simply can fall through, if the API version change did not affect this
     * endpoint.
     */
    switch ($version) {
      case '1.0':
        return $this->userV1v0($uuid);

      default:
        return $this->invalidAcceptHeaderResponse('Invalid version specified in accept header.' . $method);
    }
  }

  /**
   * Callback for API endpoint /users/UUID v1.0.
   */
  public function userV1v0($uuid) {
    $users = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['uuid' => $uuid]);
    if (empty($users)) {
      return JsonResponse::create([
        'type' => 'not_found',
        'title' => 'UUID not found.',
        'status' => 404,
      ], 404);
    }
    $user = reset($users);
    $data = new \stdClass();
    if ($this->moduleHandler->moduleExists('usage_stats')) {
      $request = $this->requestStack->getCurrentRequest();
      $since = (int) $request->get('since', 0);
      $kic_api_settings = $this->configFactory->get('kic_api.settings');
      $node_type = $kic_api_settings->get('course_node_type');

      $node_visits = usage_stats_user_route_requests($user->id(), $since,
        'entity.node.canonical');
      $nids = [];
      $visit_timestamps = [];
      foreach ($node_visits as $v) {
        if (empty($v->parameter)) {
          continue;
        }
        $nid = unserialize($v->parameter)['node'] ?? 0;
        $nids[$nid] = $nid;
        $visit_timestamps[$nid][] = $v->timestamp;
      }
      $data->course_visits = [];
      if (!empty($nids)) {
        $query = $this->dbconn->select('node', 'n')
          ->fields('n', ['nid', 'uuid']);
        $alias_nfd = $query->join('node_field_data', 'nfd', 'n.nid = %alias.nid');
        $courses = $query->condition("$alias_nfd.nid", $nids, 'IN')
          ->condition("$alias_nfd.type", $node_type)
          ->execute()
          ->fetchAllAssoc('nid');
        foreach ($courses as $nid => $c) {
          $data->course_visits[] = (object) [
            'uuid' => $c->uuid,
            'timestamps' => $visit_timestamps[$nid],
          ];
        }
      }

      // @todo Make these two configurable.
      $search_form_selector = '#views_exposed_form__search_content';
      $search_form_input_field = 'keys';
      $form_subs = usage_stats_user_form_submissions($user->id(), $since,
        $search_form_selector);
      $search_terms = [];
      foreach ($form_subs as $s) {
        $search_term
          = unserialize($s->form_data)[$search_form_input_field] ?? '';
        if (empty($search_term)) {
          continue;
        }
        $hash_base = $search_term;
        $hash = crc32($hash_base);
        while (isset($search_terms[$hash])
          && $search_terms[$hash]->search_term !== $search_term) {

          $hash_base .= 'x';
          $hash = crc32($hash_base);
        }
        if (!isset($search_terms[$hash])) {
          $search_terms[$hash] = (object) [
            'search_term' => $search_term,
          ];
        }
        $search_terms[crc32($search_term)]->timestamps[] = $s->timestamp;
      }
      $data->search_terms = array_values($search_terms);

      // @todo Make this configurable.
      $enroll_action = 'open_course';
      $actions = usage_stats_user_misc_actions($user->id(), $since,
        $enroll_action);
      $enrollments = [];
      foreach ($actions as $a) {
        if (substr($a->data, 0, 5) === 'uuid:') {
          $uuid = substr($a->data, 5);
          $enrollments[$uuid] = (object) [
            'uuid' => $uuid,
          ];
        }
      }
      $data->enrollments = array_values($enrollments);
    }
    if ($this->moduleHandler->moduleExists('kic_learn')) {
      $saml_id = new KicLearnSimplesamlphpUserId($this->configFactory,
        $this->loggerFactory);
      $saml_uuid = $saml_id->getUserId($user);
      if (!empty($saml_uuid)) {
        $data->saml_id = $saml_uuid;
      }
    }
    return JsonResponse::create($data);
  }

}
