<?php

namespace Drupal\kic_api\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\kic_api\Normalizer\ContentEntityNormalizer;

/**
 * Configuration settings form for kic_api.
 */
class SettingsForm extends ConfigFormBase {

  const SETTINGS = 'kic_api.settings';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Constructor of FrontController.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager) {

    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kic_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config(static::SETTINGS);

    $content_types = $this
      ->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    $course_node_type_options = ['none' => $this->t('-None-')];
    $course_node_type_options += array_map(function ($t) {
      return $t->get('name');
    }, $content_types);
    $course_type = $config->get('course_node_type', 'none');
    $form['course_node_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Course Node Type'),
      '#description' => $this->t('Select the content type, which represents courses. These are exposed via the API.'),
      '#options' => $course_node_type_options,
      '#default_value' => $course_type,
    ];

    $field_map = $this->entityFieldManager->getFieldMap();
    $node_fields = array_map(function ($f) {
      return $f['type'];
    }, $field_map['node']);
    $term_fields = array_map(function ($f) {
      return $f['type'];
    }, $field_map['taxonomy_term']);

    $kic_key_field_options = [];
    foreach ($term_fields as $field => $type) {
      if ($type === 'string') {
        $kic_key_field_options[$field] = $field;
      }
    }
    $kic_key_field_options = [
      'none' => $this->t('-None-'),
    ] + $kic_key_field_options;
    $form['kic_key_field'] = [
      '#type' => 'select',
      '#title' => $this->t('KI-Campus Key Field'),
      '#description' => $this->t('Select the field that holds the key of referenced entities.'),
      '#options' => $kic_key_field_options,
      '#default_value' => $config->get('kic_key_field', 'none'),
    ];

    $kic_uuid_field_options = [];
    foreach (array_intersect_assoc($term_fields, $node_fields) as $field => $type) {
      if ($type === 'string') {
        $kic_uuid_field_options[$field] = $field;
      }
    }
    $kic_uuid_field_options = [
      'none' => $this->t('-None-'),
    ] + $kic_uuid_field_options;
    $form['kic_uuid_field'] = [
      '#type' => 'select',
      '#title' => $this->t('KI-Campus Key Field'),
      '#description' => $this->t('Select the field that holds the key of referenced entities. This field must be available for taxonomy terms and nodes.'),
      '#options' => $kic_uuid_field_options,
      '#default_value' => $config->get('kic_uuid_field', 'none'),
    ];

    $field_options = [];
    $src_type_map = ContentEntityNormalizer::SOURCE_TYPE_MAP;
    foreach ($src_type_map as $dst_type => $src_types) {
      $field_options[$dst_type] = ['none' => $this->t('-None-')];
    }
    foreach ($node_fields as $field_name => $field_type) {
      foreach ($field_options as $dst_type => &$type_fields) {
        if (in_array($field_type, $src_type_map[$dst_type])) {
          $type_fields[$field_name] = $field_name;
        }
      }
    }

    $form['course_fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Course Fields'),
      '#description' => $this->t('Map the data fields of course content.'),
      '#tree' => TRUE,
    ];
    $field_map = $config->get('course_fields', []);
    foreach (ContentEntityNormalizer::COURSE_TYPE_MAP as $data_field => $data_type) {
      if (empty($data_type)) {
        continue;
      }
      $data_type = explode(' ', $data_type)[0];
      if (count($field_options[$data_type]) <= 1) {
        continue;
      }
      $default_value = $field_map[$data_field] ?? 'none';
      $form['course_fields'][$data_field] = [
        '#type' => 'select',
        '#title' => $this->t('Data Field "@f"', ['@f' => $data_field]),
        '#description' => $this->t('Select the source field for the data field "@f" of the API.', ['@f' => $data_field]),
        '#options' => $field_options[$data_type],
        '#default_value' => $default_value,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /* Validate the course data. */
    $content_types = $this
      ->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    $course_node_type = $form_state->getValue('course_node_type');
    if (!isset($content_types[$course_node_type])) {
      $form_state->setErrorByName('course_node_type',
        $this->t('Invalid node type.'));
    }

    $field_map = $this->entityFieldManager->getFieldMap();

    $course_fields = $form_state->getValue('course_fields');
    foreach (ContentEntityNormalizer::COURSE_TYPE_MAP as $data_field => $data_type) {
      $src_field = $course_fields[$data_field] ?? 'none';
      if ($src_field === 'none') {
        $course_fields[$data_field] = 'none';
        continue;
      }
      if (!in_array($course_node_type, $field_map['node'][$src_field]['bundles'])) {
        $form_state->setErrorByName('course_fields][' . $data_field,
          $this->t("The selected field is not part of the course node type's bundle."));
      }
    }
    $form_state->setValue('course_fields', $course_fields);

    /* Validate the kic key field. */
    $kic_key_field = $form_state->getValue('kic_key_field');
    if (!empty($kic_key_field) && $kic_key_field !== 'none'
      && $field_map['taxonomy_term'][$kic_key_field]['type'] !== 'string') {

      $form_state->setErrorByName('kic_key_field',
        $this->t('The KI-Campus Key Field has an invalid type.'));
    }

    /* Validate the kic UUID field. */
    $kic_uuid_field = $form_state->getValue('kic_uuid_field');
    if (!empty($kic_uuid_field) && $kic_uuid_field !== 'none'
      && ($field_map['taxonomy_term'][$kic_uuid_field]['type'] !== 'string'
      || $field_map['node'][$kic_uuid_field]['type'] !== 'string')) {

      $form_state->setErrorByName('kic_uuid_field',
        $this->t('The KI-Campus UUID Field has an invalid type.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $config->set('course_node_type', $form_state->getValue('course_node_type'));
    $config->set('course_fields', $form_state->getValue('course_fields'));
    $config->set('kic_key_field', $form_state->getValue('kic_key_field'));
    $config->set('kic_uuid_field', $form_state->getValue('kic_uuid_field'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

}
