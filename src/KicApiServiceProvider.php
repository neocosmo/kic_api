<?php

namespace Drupal\kic_api;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Adds kic+json as known format.
 */
class KicApiServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->has('http_middleware.negotiation')
      && is_a($container->getDefinition('http_middleware.negotiation')->getClass(), '\Drupal\Core\StackMiddleware\NegotiationMiddleware', TRUE)) {

      $container
        ->getDefinition('http_middleware.negotiation')
        ->addMethodCall('registerFormat', ['kic_json', ['application/kic+json']]);
    }
  }

}
