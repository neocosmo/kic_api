<?php

namespace Drupal\kic_api\Normalizer;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Normalizer for KIC API entities.
 */
class ContentEntityNormalizer extends NormalizerBase implements ContainerInjectionInterface {

  /*
   * Map which source field types work with which data retrieval method.
   */
  const SOURCE_TYPE_MAP = [
    'value' => ['string', 'text_long', 'string_long', 'integer'],
    'ref' => ['entity_reference', 'entity_reference_revisions'],
    'timestamp' => ['timestamp'],
    'date' => ['datetime'],
    'property' => [],
    'custom' => [],
  ];

  /*
   * The keys of this map represent how the data for the field is retrieved from
   * the source field.
   */
  const COURSE_TYPE_MAP = [
    /* Property. */
    'title' => 'property function getTitle',
    'language' => 'property function language',
    /* Single plain value. */
    'id' => 'value',
    'abstract' => 'value',
    'description' => 'value',
    'ects' => 'value',
    'duration' => 'value',
    'course_code' => 'value',
    /* Single key reference. */
    'target_group' => 'ref taxonomy_term kic_key_field',
    'achievement_record' => 'ref taxonomy_term kic_key_field',
    'supplier' => 'ref taxonomy_term kic_key_field',
    'format' => 'ref taxonomy_term kic_key_field',
    'level' => 'ref taxonomy_term kic_key_field',
    /* Multiple key references. */
    'subject_group' => 'ref taxonomy_term kic_key_field[]',
    'occupational_field' => 'ref taxonomy_term kic_key_field[]',
    'external_knownledge' => 'ref taxonomy_term kic_key_field[]',
    'social_learning' => 'ref taxonomy_term kic_key_field[]',
    /* Date. */
    'start_date' => 'date',
    'end_date' => 'date',
    /* Single uuid reference. */
    'institution' => 'ref taxonomy_term kic_uuid_field',
    /* Multiple uuid references. */
    'lecturers' => 'ref node kic_uuid_field[]',
    /* Unimplemented. */
    'content_tags' => '',
    'associated_course_units' => '',
    'exams' => '',
    'ects_recognized_by' => '',
    'rating' => '',
  ];

  /**
   * The configuration factory to retrieve the module's configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The normalizer works on content entities.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $supportedInterfaceOrClass = ContentEntityInterface::class;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory to retrieve the module's configuration.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   A logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactory $logger_factory,
    EntityTypeManagerInterface $entity_type_manager) {

    $this->configFactory = $config_factory;
    $this->logger = $logger_factory->get('kic_api');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    $kic_api_settings = $this->configFactory->get('kic_api.settings');
    if ($entity->getType() === $kic_api_settings->get('course_node_type')) {
      return $this->normalizeCourse($entity, $kic_api_settings);
    }
    return [];
  }

  /**
   * Check whether there is any value for a given field.
   *
   * @param string $field
   *   The name of the field to access.
   * @param string $attribute
   *   The name of the attribute which is associated with the provided field.
   * @param \Drupal\node\NodeInterface $node
   *   The node entity on which to check the presence of the attribute's field.
   *
   * @return bool
   *   True, if the provided entity has the field associated with the provided
   *   attribute and that field's value is not empty. False otherwise.
   */
  protected function checkAttribute(
    string $field,
    string $attribute,
    NodeInterface $node) {

    if (empty($field) || $field === 'none') {
      // Log at debug level, because we assume the configuration to be correct
      // and missing associations to be deliberate.
      $this->logger
        ->debug('Attribute "@attribute" of course misses an associated field.',
          ['@attribute' => $attribute]);
      return FALSE;
    }

    if (!$node->hasField($field)) {
      // Log as warning because the mapping between attributes and fields is
      // done via configuration and therefore we assume the mapping to be
      // correct.
      $this->logger
        ->warning('Missing field "@field" in content type "@type".',
          ['@field' => $field, '@type' => $node->getType()]);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Normalize course data.
   *
   * @param \Drupal\node\NodeInterface $course
   *   The course node that is normalized.
   * @param \Drupal\Core\Config\ImmutableConfig $kic_api_settings
   *   The module's configuration.
   *
   * @return array
   *   The normalized course data.
   */
  protected function normalizeCourse(
    NodeInterface $course,
    ImmutableConfig $kic_api_settings) {

    $normalized = [];
    $course_fields = $kic_api_settings->get('course_fields', []);
    foreach (static::COURSE_TYPE_MAP as $attribute => $attribute_type) {
      $multiple = FALSE;
      if (substr($attribute_type, -2) === '[]') {
        $multiple = TRUE;
        $attribute_type = substr($attribute_type, 0, -2);
      }
      $attribute_type_cmps = explode(' ', $attribute_type);
      $base_type = array_shift($attribute_type_cmps);

      if ($base_type === 'property') {
        if ($attribute_type_cmps[0] === 'function') {
          $normalized[$attribute] = $course->{$attribute_type_cmps[1]}();

          if ($attribute === 'language') {
            $normalized[$attribute] = $normalized[$attribute]->getId();
          }
        }
        elseif ($attribute_type_cmps[0] === 'value') {
          $normalized[$attribute] = $course->{$attribute_type_cmps[1]};
        }
        continue;
      }

      $field = $course_fields[$attribute] ?? 'none';
      if (!$this->checkAttribute($field, $attribute, $course)) {
        continue;
      }

      $field_values = $course->get($field);
      $attribute_values = [];

      if ($base_type === 'value') {
        foreach ($field_values as $field_value) {
          $attribute_values[] = $field_value->getValue()['value'];
        }
      }
      elseif ($base_type === 'ref') {
        foreach ($field_values as $field_value) {
          $entity_id = $field_value->getValue()['target_id'] ?? 0;
          $ref_data = $this->getEntityRefData($entity_id,
            $attribute_type_cmps[0], $attribute_type_cmps[1]);
          if (!empty($ref_data)) {
            $attribute_values[] = $ref_data;
          }
        }
      }
      elseif ($base_type === 'timestamp') {
        foreach ($field_values as $field_value) {
          $timestamp = $field_value->getValue()['value'] ?? 0;
          $attribute_values[] = date('c', $timestamp);
        }
      }
      elseif ($base_type === 'date') {
        foreach ($field_values as $field_value) {
          $attribute_values[] = $field_value->date->format('c');
        }
      }

      if ($multiple) {
        $normalized[$attribute] = $attribute_values;
      }
      else {
        if (empty($attribute_values)) {
          $normalized[$attribute] = '';
        }
        else {
          $normalized[$attribute] = array_shift($attribute_values);
        }
      }
    }
    return $normalized;
  }

  /**
   * Get data from a referenced entity.
   *
   * @param int $entity_id
   *   The ID of the entity we get the data from.
   * @param string $entity_type
   *   The type of the entity to load.
   * @param string $ref_field
   *   The configuration entry, which denotes the field from which the data is
   *   loaded.
   *
   * @return string
   *   The data from the field.
   */
  protected function getEntityRefData(
    int $entity_id,
    string $entity_type,
    string $ref_field) {

    $config = $this->configFactory->get('kic_api.settings');
    $field = $config->get($ref_field);

    if (empty($field)) {
      $this->logger->error('Undefined field name for type @t.',
        ['@t' => $ref_field]);
      return '';
    }

    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    if (!$entity) {
      $this->logger
        ->error('Failed to entity "@t".', ['@t' => $entity_id]);
      return '';
    }
    if (!$entity->hasField($field)) {
      $this->logger->error('Missing field "@f" on taxonomy term "@t".',
          ['@t' => $entity_id, '@f' => $field]);
      return '';
    }
    $values = $entity->get($field);
    if ($values->isEmpty()) {
      $this->logger->error('KIC key field "@f" of taxonomy term "@t" is empty.',
          ['@t' => $term_id, '@f' => $field]);
      return '';
    }
    return $values->first()->getValue()['value'] ?? '';
  }

}
