<?php

namespace Drupal\kic_api\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase as SerializationNormalizerBase;

/**
 * Base class for Normalizers.
 */
abstract class NormalizerBase extends SerializationNormalizerBase {

  /**
   * List of formats which supports (de-)normalization.
   *
   * @var string|string[]
   */
  protected $format = ['kic_json'];

}
